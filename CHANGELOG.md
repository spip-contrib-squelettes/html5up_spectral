# Changelog

## 3.0.2 - 2023-07-02

### Fixed

- #31 Ne pas limiter à 36 le nombre d'articles affichés sur la page rubrique

## 3.0.1 - 2023-06-02

### Changed

- Ne pas afficher la rubrique feature dans les rubriques choisies pour avoir un affichage cohérent avec l'affichage par défaut des secteurs 2648863c58915ed0d93161f9bf2c0304128b43a8
- Utiliser l'ordre défini dans le formulaire de configuration lorsqu'on choisit les rubriques 4b4bcbd8ea512a6af9794e7a61e61ac4a23d519d
- Suppression de l'entrée dans le formulaire de configuration 2648863c58915ed0d93161f9bf2c0304128b43a8
- Ne pas afficher le descritif du site dans l'entête de l'accueil
- Ne pas nécessiter le plugin Pages ref #26

## 3.0.0 - 2023-06-02

### Fixed

- Suppression de la fonctionnalité permettant de choisir le Titre page accueil
- #29 La saisie palette de couleur couleur_typo n’affiche pas la bonne couleur par défaut

### Changed

- #27 Compatibilité SPIP 4.1+
- #27 Utiliser le plugin Menus dans le pied de page à la place de la configuration du squelette
- #27 Renommer la feuille de styles du squelette
- #26 Supprimer l'article accueil sur l'accueil et afficher le descriptif du site à la place
- Utiliser le plungin Fontawsome au lieu de la police Fontawsome
- Trier les rubriques choisies sur l'accueil par num titre
- Faire fonctionner les modèles bouton et icone 
- Simplifier le bouton "Lire la suite" sur l'accueil
- Utiliser des inclure sur l'accueil pour faciliter les surcharges
- Utiliser l'inclure du plugin Sociaux au lieu d'un inclure local
- #2 Pourvoir choisir ce qu'on affiche dans la grille de la page d'accueil
- Ne pas limiter l'affichage des rubriques de l'accueil à 5
- #21 Transprence du pied de page uniquement sur l'accueil
- Hiérarchie des titres sur l'accueil
- Limiter la largeur du contenu de l'entête sur l'accueil