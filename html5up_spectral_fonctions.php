<?php
/**
 * Fonctions utiles au plugin Html5up Spectral 
 *
 * @plugin     Html5up Spectral 
 * @copyright  2020
 * @author     chankalan
 * @licence    GNU/GPL
 * @package    SPIP\Html5up_spectral\Fonctions
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}


/**
 * Pouvoir utiliser un paramètre de configuration de couleur hexa dans un css rgba
 * 
 * cf css/html5up_spectral.spip.css.html
 * [.arbo { color:rgba((#CONFIG{html5up/couleur_typo}|html5up_hex2dec),0.5) !important; }]
 *
 */
function html5up_hex2dec($hex){
	include_spip('inc/filtres_images_lib_mini');
	$dec = _couleur_hex_to_dec($hex);
	$rgb = $dec['red'].','.$dec['green'].','.$dec['blue'];
	return $rgb;
}
